import 'package:flutter/material.dart';
import 'package:open_library/models/ol_book_model.dart';
import 'package:open_library/models/ol_search_model.dart';
import 'package:open_library/open_library.dart';

const ISBN1 = '9783608980806';
const ISBN2 = '0674995171';
const ISBN3 = '3596191130';

class BookScreen extends StatefulWidget {
  const BookScreen({Key? key}) : super(key: key);

  @override
  State<BookScreen> createState() => _BookScreenState();
}

class _BookScreenState extends State<BookScreen> {
  final TextEditingController controller =
      TextEditingController(text: "Lord of the Rings");
  late bool isLoading = false;

  final OpenLibrary _openLibrary = OpenLibrary();

  final List<OLBook> _books = [];

  @override
  void dispose() {
    _openLibrary.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      body: _bodyWidget(),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          setState(() {
            isLoading = true;
          });
          _books.clear();
          const bool loadCovers = true;
          const CoverSize size = CoverSize.S;
          final OLBookBase book1 = await _openLibrary.getBookByISBN(
              isbn: ISBN1, loadCover: loadCovers, coverSize: size);
          debugPrint(book1.toString());
          if (book1 is OLBook) {
            _books.add(book1);
          }
          final OLBookBase book2 = await _openLibrary.getBookByISBN(
              isbn: ISBN2, loadCover: loadCovers, coverSize: size);
          debugPrint(book2.toString());
          if (book2 is OLBook) {
            _books.add(book2);
          }
          final OLBookBase book3 = await _openLibrary.getBookByISBN(
              isbn: ISBN3, loadCover: loadCovers, coverSize: size);
          debugPrint(book3.toString());
          if (book3 is OLBook) {
            _books.add(book3);
          }
          setState(() => isLoading = false);
        },
        child: const Icon(Icons.book),
      ),
    );
  }

  Widget _bodyWidget() {
    if (isLoading) {
      return const Center(
        child: CircularProgressIndicator(
          color: Colors.white,
        ),
      );
    }

    return Column(
      children: [
        const SizedBox(height: 50.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * 0.6,
              height: 60.0,
              child: TextField(
                controller: controller,
                cursorColor: Colors.white,
                style: const TextStyle(color: Colors.white),
                decoration: const InputDecoration(
                  label: Text("Book title:"),
                  labelStyle: TextStyle(color: Colors.white),
                  focusColor: Colors.white,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: GestureDetector(
                onTap: _onSearchTapped,
                child: const Icon(
                  Icons.search,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 20.0,
        ),
        const Center(
            child: Text(
          "press action button to search for those ISBN's",
          style: TextStyle(color: Colors.white),
        )),
        const SizedBox(
          height: 20.0,
        ),
        const Center(
            child: Text(
          "ISBN1:$ISBN1",
          style: TextStyle(color: Colors.white),
        )),
        const SizedBox(height: 20.0),
        const Center(
            child: Text(
          "ISBN2:$ISBN2",
          style: TextStyle(color: Colors.white),
        )),
        const SizedBox(height: 20.0),
        const Center(
            child: Text(
          "ISBN3:$ISBN3",
          style: TextStyle(color: Colors.white),
        )),
        const SizedBox(height: 20.0),
        SingleChildScrollView(
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: _books.length,
            itemBuilder: (context, index) {
              return bookWidget(book: _books[index], context: context);
            },
          ),
        )
      ],
    );
  }

  Widget bookWidget({required OLBook book, required BuildContext context}) {
    String author = '';
    if (book.authors.isNotEmpty) {
      author = book.authors.first.name.trim();
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: 80.0,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.only(top: 4.0, bottom: 4.0, left: 20.0),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Text(
                      book.title,
                      overflow: TextOverflow.ellipsis,
                      style: const TextStyle(
                          color: Colors.black,
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(top: 4.0, bottom: 4.0, left: 20.0),
                  child: Text(
                    author,
                    style: const TextStyle(color: Colors.black, fontSize: 12),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: SizedBox(
                height: book.covers.isNotEmpty ? 64.0 : 0,
                child: book.covers.isNotEmpty
                    ? Image.memory(book.covers.first)
                    : null,
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _onSearchTapped() async {
    setState(() => isLoading = true);

    _openLibrary.query(title: controller.text, limit: 4).then((value) {
      _books.clear();
      debugPrint("search:\n$value");
      if (value is OLSearch) {
        for (var doc in value.docs) {
          final OLBook book = OLBook(
            title: doc.title,
            authors: doc.authors,
            covers: doc.covers,
            language: doc.language,
          );
          debugPrint("book: ${book.toString()}");
          _books.add(book);
        }

        setState(() => isLoading = false);
      }
    }).onError((error, stackTrace) {
      var bar = SnackBar(content: Text("Failed to get data: $error"));
      ScaffoldMessenger.of(context).showSnackBar(bar);
    });
  }
}
