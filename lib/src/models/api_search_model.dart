import 'package:json_annotation/json_annotation.dart';
import 'package:open_library/src/models/api_search_doc_model.dart';

import '../collections.dart';

part 'api_search_model.g.dart';

abstract class APISearchBase {
  const APISearchBase();
}

class APISearchError extends APISearchBase {
  static const message = "Error on returning valid Search Result Objects";

  const APISearchError();

  @override
  String toString() {
    return 'Error: $message';
  }
}

@JsonSerializable()
class APISearch extends APISearchBase {
  final int numFound;
  final bool numFoundExact;
  final String q;
  final List<APISearchDoc> docs;

  @override
  bool operator ==(Object other) =>
      other is APISearch &&
      numFound == other.numFound &&
      numFoundExact == other.numFoundExact &&
      q == other.q &&
      listEquals(docs, other.docs);

  @override
  int get hashCode =>
      numFound.hashCode ^ numFoundExact.hashCode ^ q.hashCode ^ docs.hashCode;

  const APISearch({
    this.numFound = 0,
    this.numFoundExact = false,
    this.q = "",
    this.docs = const [],
  });

  factory APISearch.fromJson(Map<String, dynamic> json) {
    return _$APISearchFromJson(json);
  }

  Map<String, dynamic> toJson() => _$APISearchToJson(this);

  @override
  String toString() {
    return '\nSearch:\n  numFound:$numFound\n  numFoundExact:$numFoundExact\n q:$q numDocs:${docs.length}';
  }
}
