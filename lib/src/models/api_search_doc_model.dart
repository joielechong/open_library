import 'package:json_annotation/json_annotation.dart';
import 'package:open_library/src/collections.dart';

part 'api_search_doc_model.g.dart';

abstract class APISearchDocBase {
  const APISearchDocBase();
}

class APISearchError extends APISearchDocBase {
  static const message = "Error on returning valid SearchDoc Object";

  const APISearchError();

  @override
  String toString() {
    return 'Error: $message';
  }
}

@JsonSerializable()
class APISearchDoc extends APISearchDocBase {
  final String key;
  final String type;
  final List<String> seed;
  final String title;
  final String title_suggest;
  final bool has_fulltext;
  final int edition_count;
  final List<String> edition_key;
  final List<String> publish_date;
  final List<int> publish_year;
  final int first_publish_year;
  final int number_of_pages_median;
  final List<String> lccn;
  final List<String> publish_place;
  final List<String> oclc;
  final List<String> contributor;
  final List<String> lcc;
  final List<String> ddc;
  final List<String> isbn;
  final int last_modified_i;
  final int ebook_count_i;
  final List<String> ia;
  final bool public_scan_b;
  final String ia_collection_s;
  final String lending_edition_s;
  final String lending_identifier_s;
  final String printdisabled_s;
  final String cover_edition_key;
  final int cover_i;
  final List<String> first_sentence;
  final List<String> publisher;
  final List<String> language;
  final List<String> author_key;
  final List<String> author_name;
  final List<String> author_alternative_name;
  final List<String> person;
  final List<String> place;
  final List<String> subject;
  final List<String> time;
  final List<String> id_alibris_id;
  final List<String> id_amazon;
  final List<String> id_bcid;
  final List<String> id_dnb;
  final List<String> id_goodreads;
  final List<String> id_google;
  final List<String> id_librarything;
  final List<String> id_libris;
  final List<String> id_overdrive;
  final List<String> id_wikidata;
  final List<String> ia_loaded_id;
  final List<String> ia_box_id;
  final List<String> publisher_facet;
  final List<String> person_key;
  final List<String> place_key;
  final List<String> time_facet;
  final List<String> person_facet;
  final List<String> subject_facet;
  final List<String> place_facet;
  final List<String> author_facet;
  final List<String> subject_key;
  final List<String> time_key;
  final String lcc_sort;
  final String ddc_sort;

  @override
  bool operator ==(Object other) =>
      other is APISearchDoc &&
      key == other.key &&
      type == other.title &&
      listEquals(seed, other.seed) &&
      title == other.title &&
      title_suggest == other.title_suggest &&
      has_fulltext == other.has_fulltext &&
      edition_count == other.edition_count &&
      listEquals(edition_key, other.edition_key) &&
      listEquals(publish_date, other.publish_date) &&
      listEquals(publish_year, other.publish_year) &&
      first_publish_year == other.first_publish_year &&
      number_of_pages_median == other.number_of_pages_median &&
      listEquals(lcc, other.lccn) &&
      listEquals(publish_place, other.publish_place) &&
      listEquals(oclc, other.oclc) &&
      listEquals(contributor, other.contributor) &&
      listEquals(lcc, other.lcc) &&
      listEquals(ddc, other.ddc) &&
      listEquals(isbn, other.isbn) &&
      last_modified_i == other.last_modified_i &&
      ebook_count_i == other.ebook_count_i &&
      listEquals(ia, other.ia) &&
      public_scan_b == other.public_scan_b &&
      ia_collection_s == other.ia_collection_s &&
      lending_edition_s == other.lending_edition_s &&
      lending_identifier_s == other.lending_identifier_s &&
      printdisabled_s == other.printdisabled_s &&
      cover_edition_key == other.cover_edition_key &&
      cover_i == other.cover_i &&
      listEquals(first_sentence, other.first_sentence) &&
      listEquals(publisher, other.publisher) &&
      listEquals(language, other.language) &&
      listEquals(author_key, other.author_key) &&
      listEquals(author_name, other.author_name) &&
      listEquals(author_alternative_name, other.author_alternative_name) &&
      listEquals(person, other.person) &&
      listEquals(place, other.place) &&
      listEquals(subject, other.subject) &&
      listEquals(time, other.time) &&
      listEquals(id_alibris_id, other.id_alibris_id) &&
      listEquals(id_amazon, other.id_amazon) &&
      listEquals(id_bcid, other.id_bcid) &&
      listEquals(id_dnb, other.id_dnb) &&
      listEquals(id_goodreads, other.id_goodreads) &&
      listEquals(id_google, other.id_google) &&
      listEquals(id_librarything, other.id_librarything) &&
      listEquals(id_libris, other.id_libris) &&
      listEquals(id_overdrive, other.id_overdrive) &&
      listEquals(id_wikidata, other.id_wikidata) &&
      listEquals(ia_loaded_id, other.ia_loaded_id) &&
      listEquals(ia_box_id, other.ia_box_id) &&
      listEquals(publisher_facet, other.publisher_facet) &&
      listEquals(person_key, other.person_key) &&
      listEquals(place_key, other.place_key) &&
      listEquals(time_facet, other.time_facet) &&
      listEquals(person_facet, other.person_facet) &&
      listEquals(subject_facet, other.subject_facet) &&
      listEquals(place_facet, other.place_facet) &&
      listEquals(author_facet, other.author_facet) &&
      listEquals(subject_key, other.subject_key) &&
      listEquals(time_key, other.time_key) &&
      lcc_sort == other.lcc_sort &&
      ddc_sort == other.ddc_sort &&
      listEquals(language, other.language);

  @override
  int get hashCode =>
      key.hashCode ^
      type.hashCode ^
      seed.hashCode ^
      title.hashCode ^
      title_suggest.hashCode ^
      has_fulltext.hashCode ^
      edition_count.hashCode ^
      edition_key.hashCode ^
      publish_date.hashCode ^
      publish_year.hashCode ^
      first_publish_year.hashCode ^
      number_of_pages_median.hashCode ^
      lccn.hashCode ^
      publish_place.hashCode ^
      oclc.hashCode ^
      contributor.hashCode ^
      lcc.hashCode ^
      ddc.hashCode ^
      isbn.hashCode ^
      last_modified_i.hashCode ^
      ebook_count_i.hashCode ^
      ia.hashCode ^
      public_scan_b.hashCode ^
      ia_collection_s.hashCode ^
      lending_edition_s.hashCode ^
      lending_identifier_s.hashCode ^
      printdisabled_s.hashCode ^
      cover_edition_key.hashCode ^
      cover_i.hashCode ^
      first_sentence.hashCode ^
      publisher.hashCode ^
      language.hashCode ^
      author_key.hashCode ^
      author_name.hashCode ^
      author_alternative_name.hashCode ^
      person.hashCode ^
      place.hashCode ^
      subject.hashCode ^
      time.hashCode ^
      id_alibris_id.hashCode ^
      id_amazon.hashCode ^
      id_bcid.hashCode ^
      id_dnb.hashCode ^
      id_goodreads.hashCode ^
      id_google.hashCode ^
      id_librarything.hashCode ^
      id_libris.hashCode ^
      id_overdrive.hashCode ^
      id_wikidata.hashCode ^
      ia_loaded_id.hashCode ^
      ia_box_id.hashCode ^
      publisher_facet.hashCode ^
      person_key.hashCode ^
      place_key.hashCode ^
      time_facet.hashCode ^
      person_facet.hashCode ^
      subject_facet.hashCode ^
      place_facet.hashCode ^
      author_facet.hashCode ^
      subject_key.hashCode ^
      time_key.hashCode ^
      lcc_sort.hashCode ^
      ddc_sort.hashCode ^
      language.hashCode;

  const APISearchDoc({
    this.key = "",
    this.type = "",
    this.seed = const [],
    this.title = "",
    this.title_suggest = "",
    this.has_fulltext = true,
    this.edition_count = 0,
    this.edition_key = const [],
    this.publish_date = const [],
    this.publish_year = const [],
    this.first_publish_year = 0,
    this.number_of_pages_median = 0,
    this.lccn = const [],
    this.publish_place = const [],
    this.oclc = const [],
    this.contributor = const [],
    this.lcc = const [],
    this.ddc = const [],
    this.isbn = const [],
    this.last_modified_i = 0,
    this.ebook_count_i = 0,
    this.ia = const [],
    this.public_scan_b = false,
    this.ia_collection_s = "",
    this.lending_edition_s = "",
    this.lending_identifier_s = "",
    this.printdisabled_s = "",
    this.cover_edition_key = "",
    this.cover_i = 0,
    this.first_sentence = const [],
    this.publisher = const [],
    this.language = const [],
    this.author_key = const [],
    this.author_name = const [],
    this.author_alternative_name = const [],
    this.person = const [],
    this.place = const [],
    this.subject = const [],
    this.time = const [],
    this.id_alibris_id = const [],
    this.id_amazon = const [],
    this.id_bcid = const [],
    this.id_dnb = const [],
    this.id_goodreads = const [],
    this.id_google = const [],
    this.id_librarything = const [],
    this.id_libris = const [],
    this.id_overdrive = const [],
    this.id_wikidata = const [],
    this.ia_loaded_id = const [],
    this.ia_box_id = const [],
    this.publisher_facet = const [],
    this.person_key = const [],
    this.place_key = const [],
    this.time_facet = const [],
    this.person_facet = const [],
    this.subject_facet = const [],
    this.place_facet = const [],
    this.author_facet = const [],
    this.subject_key = const [],
    this.time_key = const [],
    this.lcc_sort = "",
    this.ddc_sort = "",
  });

  factory APISearchDoc.fromJson(Map<String, dynamic> json) =>
      _$APISearchDocFromJson(json);

  Map<String, dynamic> toJson() => _$APISearchDocToJson(this);

  @override
  String toString() {
    final StringBuffer sb = StringBuffer('\nSearchDoc:');
    return sb.toString();
  }
}
